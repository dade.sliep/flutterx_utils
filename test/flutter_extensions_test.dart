// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

void main() => group('Flutter Extension', () {
      test('testIterable', testIterable);
      test('testList', testList);
      test('testBlend', testBlend);
      test('testLightness', testLightness);
      test('testLoggable', testLoggable);
      test('testJsonPath', testJsonPath);
    });

void testIterable() {
  final elements = List.generate(10000, (index) => index);
  var start = DateTime.now();
  for (var i = 0; i < 10000; i++) elements.map((e) => e).toList();
  print(DateTime.now().difference(start).inMilliseconds);
  start = DateTime.now();
  for (var i = 0; i < 10000; i++) elements.map((e) => e).toList();
  print(DateTime.now().difference(start).inMilliseconds);
  var list = ['Hello', 'World'];
  expect(list.countWhere((item) => item.contains('W')), 1);
  expect(list.count('World'), 1);
  expect(list.any((item) => item.contains('W')), true);
  expect(list.contains('World'), true);
  list
    ..add('World')
    ..add('World')
    ..add('World')
    ..add('World')
    ..add('LoL');
  expect(list.count('World'), 5);
  list = list.toSet().toList();
  expect(list.count('World'), 1);
  list = list.distinct((item) => item.contains('o')).toList();
  expect(list.length, 1);
  expect(list.count('World'), 0);
  expect(MyEnum.values.optFromName('due'), MyEnum.due);
  expect(MyEnum.values.fromName('sei', fallback: MyEnum.tre), MyEnum.tre);
  expect(MyEnum.values.require((item) => item.toString() == 'MyEnum.due', message: () => 'oh no'), MyEnum.due);
  final json = {'value': 'tre'};
  expect(json.optEnum('value', MyEnum.values), MyEnum.tre);
  final values = [102, 33, 4, 6, 1, 5, -30, 6, 7, 88, 9.0];
  expect(values.closestTo(17), 9);
  expect(values.closestTo(10), 9);
  expect(values.closestTo(7.4), 7);
  expect(values.closestTo(-10), 1);
  expect(values.closestTo(-20), -30);
  expect(values.closestTo(99920), 102);
}

void testList() {
  const l = ['a', 'b', 'c'];
  var current = l[0];
  expect(current, l[0]);
  current = l.nextElement(current);
  expect(current, l[1]);
  current = l.nextElement(current);
  expect(current, l[2]);
  current = l.nextElement(current);
  expect(current, l[0]);
  current = l.nextElement('d');
  expect(current, l[0]);

  expect(MyEnum.values.optFromName('uno'), MyEnum.uno);
  expect(MyEnum.values.fromName('due', fallback: MyEnum.uno), MyEnum.due);
  expect(MyEnum.values.fromName('cinque', fallback: MyEnum.uno), MyEnum.uno);
  expect(MyEnum.values.optFromName('Uno'), null);
  expect(MyEnum.values.optFromName('Uno', ignoreCase: true), MyEnum.uno);
  expect(MyEnum.values.requireFromName('tre'), MyEnum.tre);

  const dst = [
    Item(0, 'Paolo', 17),
    Item(1, 'Luca', 18),
    Item(2, 'Giovanna', 24),
  ];
  const src = [
    Item(0, 'Giuseppe', 33),
    Item(2, 'Giorno', 24),
    Item(3, 'Maura', 67),
  ];
  const exp = [
    Item(0, 'Giuseppe (updated)', 33),
    Item(1, 'Luca', 18),
    Item(2, 'Giorno (updated)', 24),
    Item(3, 'Maura (new)', 67),
  ];
  final res = dst.merge(src,
      condition: (a, b) => a.id == b.id,
      whenMatchedUpdate: (oi, ni) => Item(ni.id, '${ni.name} (updated)', ni.age),
      whenNotMatchedInsert: (ni) => Item(ni.id, '${ni.name} (new)', ni.age));
  expect(res, exp);
}

@immutable
class Item {
  final int id;
  final String name;
  final int age;

  const Item(this.id, this.name, this.age);

  @override
  bool operator ==(Object other) => other is Item && other.id == id && other.name == name && other.age == age;

  @override
  int get hashCode => Object.hash(id, name, age);

  @override
  String toString() => '$id, $name, $age';
}

enum MyEnum {
  uno,
  due,
  tre,
}

void testBlend() {
  const red = Color(0xffff0000);
  const yellow = Color(0xffffff00);
  const blue = Color(0xff0000ff);
  expect(Color.lerp(yellow, red, .5), const Color(0xffff7f00));
  expect(Color.lerp(blue, Colors.white, .6), const Color(0xff9999ff));
  expect(Color.lerp(blue, Colors.white, .2), const Color(0xff3333ff));
}

void testLightness() {
  final grey87 = Color.lerp(Colors.white, Colors.black, .87);
  expect(grey87, const Color(0xff212121));
}

void testLoggable() => LoggableTest().start();

class LoggableTest with Loggable {
  void start() => logInfo('Welcome to loggable test', () {
        logInfo('Indented text');
        var result = const Result.success('Hello');
        logInfo(result);
        logInfo(result.resultOrThrow);
        result = Result.wrap(() => throw ArgumentError('Invalid argument'));
        logInfo(result);
      });
}

void testJsonPath() {
  final map = {
    'e1': {
      'e2': [
        {'e3a': 123},
        {'e3b': 456},
      ],
      'e4': true
    }
  };

  print('root: ${JsonPath.root.get(map)}');
  print('e1: ${JsonPath.value('e1').get(map)}');
  print('e2: ${const JsonPath(['e1', 'e2']).get(map)}');
  print('e3a: ${const JsonPath(['e1', 'e2', 0, 'e3a']).get(map)}');
  print('e3b: ${const JsonPath(['e1', 'e2', 1, 'e3b']).get(map)}');
  print('e4: ${const JsonPath(['e1', 'e4']).get(map)}');
  const JsonPath(['e1', 'e4']).set(map, 333);
  print('e4: ${const JsonPath(['e1', 'e4']).get(map)}');
  print(map.xget('e1.e2.0.e3a'));
  map.xset('e1.e4', 666);
  print('e4: ${map.xcontains('e1.e4')}');
  print('e4: ${map.xremove('e1.e4')}');
  print('e4: ${map.xget('e1.e4')}');
  print('e4: ${map.xcontains('e1.e4')}');
}
