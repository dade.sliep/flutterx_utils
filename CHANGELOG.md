## 1.6.8

* Fix types

## 1.6.5

* Add hashMap

## 1.6.4

* Fix string ext

## 1.6.3

* Add WidgetDataSync

## 1.6.2

* Restore SafeDryLayout

## 1.6.1

* Improve ScrollableView

## 1.5.8

* Update flutter

## 1.5.7

* Add list extension

## 1.5.6

* Include RootMediaQuery in library

## 1.5.5

* Improve code style

## 1.5.4

* Improve mutex

## 1.5.1

* Fix fromJson check

## 1.5.0

* Fix int parsing

## 1.4.7

* Improve json utilities

## 1.4.5

* Add map function

## 1.4.4

* Improve SafeDryLayout

## 1.4.3

* Improve SizeObserver

## 1.4.2

* Add UI utils

## 1.4.1

* Add Future extension

## 1.4.0

* Improve mutex

## 1.3.9

* Add web utils

## 1.3.8

* Add NeverFuture

## 1.3.7

* Improvements

## 1.3.6

* Fix optInt

## 1.3.5

* Add get enum from mapping

## 1.3.4

* Fix parseBool

## 1.3.3

* Improve Json utilities

## 1.3.2

* Improve Result

## 1.3.1

* Improve runtimeType references

## 1.3.0

* Improve Mutex

## 1.2.9

* Improve CancellationToken

## 1.2.8

* Improve CancellationToken

## 1.2.7

* Add Set utility function

## 1.2.5

* Add Map extension functions

## 1.2.4

* Add utility functions

## 1.2.3

* Use debugPrint for loggable

## 1.2.2

* Improve cancellation token

## 1.2.1

* Refactoring

## 1.2.0

* Improve Loggable

## 1.1.9

* Fix mutex

## 1.1.8

* Add Mutex

## 1.1.7

* Add CancellationToken

## 1.1.6

* Improve DTO functions

## 1.1.5

* Add Symbol ext

## 1.1.4

* Add some utilities

## 1.1.3

* Improve json utils

## 1.1.2

* Add wrapIfNotNull utility function

## 1.1.1

* Improve json utility

## 1.1.0

* Initial stable release

## 1.0.14-dev

* Improve Color extensions

## 1.0.13-dev

* Improve Color extensions

## 1.0.12-dev

* Improve DTO extensions

## 1.0.11-dev

* Improve DTO extensions

## 1.0.10-dev

* Add DTO extension

## 1.0.9-dev

* Add DTO to library

## 1.0.8-dev

* Add DTO utils

## 1.0.7-dev

* Add iterable extension

## 1.0.6-dev

* Fix JsonPath get root element

## 1.0.5-dev

* Add option to mute Loggable

## 1.0.4-dev

* Add Strings extensions to library.

## 1.0.3-dev

* Improve Result.

## 1.0.2-dev

* Improve Result.

## 1.0.1-dev

* Add Result getter.

## 1.0.0-dev

* Initial release.
