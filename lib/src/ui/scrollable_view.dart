import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterx_utils/src/ui/size_observer.dart';
import 'package:vector_math/vector_math_64.dart' show Matrix4, Quad, Vector3;

/// Allows to scroll content in both horizontal and vertical directions
/// Wraps [InteractiveViewer] but adds scroll feature and also enables the use of scrollbars by
class ScrollableView extends StatefulWidget {
  /// [InteractiveViewer.clipBehavior] property. Defaults to [Clip.hardEdge]
  final Clip clipBehavior;

  /// [InteractiveViewer.panAxis] property. Defaults to false
  final PanAxis panAxis;

  /// [InteractiveViewer.boundaryMargin] property. Defaults to [EdgeInsets.zero]
  final EdgeInsets boundaryMargin;

  /// [InteractiveViewer.constrained] property. Defaults to false
  final bool constrained;

  /// [InteractiveViewer.panEnabled] property. Defaults to false
  final bool panEnabled;

  /// Horizontal scroll controller
  final ScrollController? controllerX;

  /// Vertical scroll controller
  final ScrollController? controllerY;

  /// If true and container width is larger than the content one, the latter will be constrained to match it
  final bool matchContainerX;

  /// If true and container height is larger than the content one, the latter will be constrained to match it
  final bool matchContainerY;

  /// handle viewport size change
  final ValueChanged<Size>? onViewportSize;

  /// handle child size change
  final ValueChanged<Size>? onChildSize;

  /// The content
  final Widget child;

  const ScrollableView({
    super.key,
    this.clipBehavior = Clip.hardEdge,
    this.panAxis = PanAxis.free,
    this.boundaryMargin = EdgeInsets.zero,
    this.constrained = false,
    this.panEnabled = false,
    this.controllerX,
    this.controllerY,
    this.matchContainerX = false,
    this.matchContainerY = false,
    this.onViewportSize,
    this.onChildSize,
    required this.child,
  });

  @override
  State<ScrollableView> createState() => _ScrollableViewState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(EnumProperty<Clip>('clipBehavior', clipBehavior))
      ..add(DiagnosticsProperty<PanAxis>('panAxis', panAxis))
      ..add(DiagnosticsProperty<EdgeInsets>('boundaryMargin', boundaryMargin))
      ..add(DiagnosticsProperty<bool>('constrained', constrained))
      ..add(DiagnosticsProperty<bool>('panEnabled', panEnabled))
      ..add(DiagnosticsProperty<ScrollController?>('controllerX', controllerX))
      ..add(DiagnosticsProperty<ScrollController?>('controllerY', controllerY))
      ..add(DiagnosticsProperty<bool>('matchContainerX', matchContainerX))
      ..add(DiagnosticsProperty<bool>('matchContainerY', matchContainerY))
      ..add(ObjectFlagProperty<ValueChanged<Size>?>.has('onViewportSize', onViewportSize))
      ..add(ObjectFlagProperty<ValueChanged<Size>?>.has('onChildSize', onChildSize));
  }
}

class _ScrollableViewState extends State<ScrollableView> {
  final TransformationController _transformationController = TransformationController();
  final GlobalKey _childKey = GlobalKey();
  Size _viewportSize = Size.zero;
  Size _childSize = Size.zero;
  double? _containerX;
  double? _containerY;

  @override
  void initState() {
    super.initState();
    _transformationController.addListener(_updateScrollPositions);
    widget.controllerX?.addListener(_updateScroll);
    widget.controllerY?.addListener(_updateScroll);
  }

  @override
  void didUpdateWidget(ScrollableView oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controllerX != oldWidget.controllerX) {
      oldWidget.controllerX?.removeListener(_updateScroll);
      widget.controllerX?.addListener(_updateScroll);
    }
    if (widget.controllerY != oldWidget.controllerY) {
      oldWidget.controllerY?.removeListener(_updateScroll);
      widget.controllerY?.addListener(_updateScroll);
    }
  }

  @override
  Widget build(BuildContext context) => Listener(
      onPointerSignal: _receivedPointerSignal,
      child: SizeObserver(
          onSizeChanged: _onViewportSizeChanged,
          child: RepaintBoundary(
              child: InteractiveViewer(
                  clipBehavior: widget.clipBehavior,
                  panAxis: widget.panAxis,
                  boundaryMargin: widget.boundaryMargin,
                  maxScale: 1,
                  minScale: 1,
                  scaleFactor: 1,
                  constrained: widget.constrained,
                  panEnabled: widget.panEnabled,
                  scaleEnabled: false,
                  transformationController: _transformationController,
                  child: SizedBox(
                      width: _containerX,
                      height: _containerY,
                      child: SizeObserver(
                          key: _childKey,
                          constraints: const PartialBoxConstraints.unconstrained(),
                          onSizeChanged: _onChildSizeChanged,
                          child: widget.child))))));

  @override
  void dispose() {
    _transformationController.removeListener(_updateScrollPositions);
    widget.controllerX?.removeListener(_updateScroll);
    widget.controllerY?.removeListener(_updateScroll);
    super.dispose();
  }

  void _updateScrollPositions() {
    final translation = _transformationController.value.getTranslation();
    widget.controllerX?.position.jumpTo(-translation.x);
    widget.controllerY?.position.jumpTo(-translation.y);
  }

  void _updateScroll() {
    final matrix = _transformationController.value;
    final translation =
        Vector3(-(widget.controllerX?.position.pixels ?? 0), -(widget.controllerY?.position.pixels ?? 0), 0);
    if (matrix.getTranslation() != translation)
      _transformationController.value = matrix.clone()..setTranslation(translation);
  }

  void _receivedPointerSignal(PointerSignalEvent event) {
    if (event is PointerScrollEvent) {
      var translation = -event.scrollDelta;
      if (translation == Offset.zero) return;
      translation = _alignAxis(translation, widget.panAxis);
      final oldMatrix = _transformationController.value;
      final newMatrix = _matrixTranslate(oldMatrix, translation);
      if (oldMatrix != newMatrix)
        GestureBinding.instance.pointerSignalResolver
            .register(event, (_) => _transformationController.value = newMatrix);
    }
  }

  void _onViewportSizeChanged(Size size) {
    widget.onViewportSize?.call(size);
    _updateScrollControllers(_viewportSize = size, _childSize);
  }

  void _onChildSizeChanged(Size size) {
    widget.onChildSize?.call(size);
    _updateScrollControllers(_viewportSize, _childSize = size);
  }

  void _updateScrollControllers(Size viewportSize, Size childSize) {
    final maxExtentX = childSize.width - viewportSize.width;
    final containerX = widget.matchContainerX && maxExtentX <= 0 ? viewportSize.width : null;
    widget.controllerX?.position.applyDimensions(viewportSize.width, maxExtentX);
    final maxExtentY = childSize.height - viewportSize.height;
    final containerY = widget.matchContainerY && maxExtentY <= 0 ? viewportSize.height : null;
    widget.controllerY?.position.applyDimensions(viewportSize.height, maxExtentY);
    if (containerX != _containerX || containerY != _containerY) {
      _containerX = containerX;
      _containerY = containerY;
      WidgetsBinding.instance.addPostFrameCallback((_) => setState(() {}));
    }
  }

  Matrix4 _matrixTranslate(Matrix4 matrix, Offset translation) {
    final nextMatrix = matrix.clone()..translate(translation.dx, translation.dy);
    final boundaryRect = widget.boundaryMargin.inflateRect(Offset.zero & _childSize);
    if (boundaryRect.isInfinite) return nextMatrix;
    final viewport = Offset.zero & _viewportSize;
    final nextViewport = _transformViewport(nextMatrix, viewport);
    final boundariesAabbQuad = _getAxisAlignedBoundingBox(boundaryRect);
    final offendingDistance = _exceedsBy(boundariesAabbQuad, nextViewport);
    if (offendingDistance == Offset.zero) return nextMatrix;
    final nextTranslation = nextMatrix.getTranslation();
    final nextTotalTranslation = Offset(nextTranslation.x, nextTranslation.y);
    final correctedTotalTranslation =
        Offset(nextTotalTranslation.dx - offendingDistance.dx, nextTotalTranslation.dy - offendingDistance.dy);
    final correctedMatrix = matrix.clone()
      ..setTranslation(Vector3(correctedTotalTranslation.dx, correctedTotalTranslation.dy, 0));
    final correctedViewport = _transformViewport(correctedMatrix, viewport);
    final offendingCorrectedDistance = _exceedsBy(boundariesAabbQuad, correctedViewport);
    if (offendingCorrectedDistance == Offset.zero) return correctedMatrix;
    if (offendingCorrectedDistance.dx != 0 && offendingCorrectedDistance.dy != 0) return matrix.clone();
    final unidirectionalCorrectedTotalTranslation = Offset(
        offendingCorrectedDistance.dx == 0 ? correctedTotalTranslation.dx : 0,
        offendingCorrectedDistance.dy == 0 ? correctedTotalTranslation.dy : 0);
    return matrix.clone()
      ..setTranslation(
          Vector3(unidirectionalCorrectedTotalTranslation.dx, unidirectionalCorrectedTotalTranslation.dy, 0));
  }

  static Quad _transformViewport(Matrix4 matrix, Rect viewport) {
    final inverseMatrix = matrix.clone()..invert();
    return Quad.points(
        inverseMatrix.transform3(Vector3(viewport.topLeft.dx, viewport.topLeft.dy, 0)),
        inverseMatrix.transform3(Vector3(viewport.topRight.dx, viewport.topRight.dy, 0)),
        inverseMatrix.transform3(Vector3(viewport.bottomRight.dx, viewport.bottomRight.dy, 0)),
        inverseMatrix.transform3(Vector3(viewport.bottomLeft.dx, viewport.bottomLeft.dy, 0)));
  }

  static Offset _exceedsBy(Quad boundary, Quad viewport) {
    final viewportPoints = <Vector3>[viewport.point0, viewport.point1, viewport.point2, viewport.point3];
    var offset = Offset.zero;
    for (final point in viewportPoints) {
      // ignore: invalid_use_of_visible_for_testing_member
      final pointInside = InteractiveViewer.getNearestPointInside(point, boundary);
      final excess = Offset(pointInside.x - point.x, pointInside.y - point.y);
      if (excess.dx.abs() > offset.dx.abs()) offset = Offset(excess.dx, offset.dy);
      if (excess.dy.abs() > offset.dy.abs()) offset = Offset(offset.dx, excess.dy);
    }
    return Offset(double.parse(offset.dx.toStringAsFixed(9)), double.parse(offset.dy.toStringAsFixed(9)));
  }

  static Quad _getAxisAlignedBoundingBox(Rect rect) {
    final rotationMatrix = Matrix4.identity();
    final boundariesRotated = Quad.points(
        rotationMatrix.transform3(Vector3(rect.left, rect.top, 0)),
        rotationMatrix.transform3(Vector3(rect.right, rect.top, 0)),
        rotationMatrix.transform3(Vector3(rect.right, rect.bottom, 0)),
        rotationMatrix.transform3(Vector3(rect.left, rect.bottom, 0)));
    // ignore: invalid_use_of_visible_for_testing_member
    return InteractiveViewer.getAxisAlignedBoundingBox(boundariesRotated);
  }

  static Offset _alignAxis(Offset translation, PanAxis panAxis) {
    switch (panAxis) {
      case PanAxis.horizontal:
        return Offset(translation.dx, 0);
      case PanAxis.vertical:
        return Offset(0, translation.dy);
      case PanAxis.aligned:
        return _alignAxis(translation, translation.dx > translation.dy ? PanAxis.horizontal : PanAxis.vertical);
      case PanAxis.free:
        return translation;
    }
  }
}

extension ScrollPositionExt on ScrollPosition {
  void applyDimensions(double viewport, double extent) => this
    ..applyViewportDimension(viewport)
    ..applyContentDimensions(0, max(extent, 0));
}
