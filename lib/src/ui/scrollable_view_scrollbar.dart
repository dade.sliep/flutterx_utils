import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

/// Allows you to apply scrollbars to any widget and control it from outside
/// This widget renders [Scrollbar] and handles [ScrollPosition] like [Scrollable] but allows you to control it
/// from outside using a [controller].
/// All you have to do in other widgets is receiving the [controller] parameter and use it after setting up the
/// position by calling [ScrollPosition.applyViewportDimension] and [ScrollPosition.applyContentDimensions] methods
class ScrollableViewScrollbar extends StatefulWidget {
  /// Direction of scroll
  final AxisDirection axisDirection;

  /// Scroll controller
  final ScrollController controller;

  /// Scroll physics
  final ScrollPhysics? physics;

  /// Scroll behaviour
  final ScrollBehavior? scrollBehavior;

  /// Restoration id
  final String? restorationId;

  /// [Scrollbar.thumbVisibility] property
  final bool? thumbVisibility;

  /// [Scrollbar.trackVisibility] property
  final bool? trackVisibility;

  /// [Scrollbar.thickness] property
  final double? thickness;

  /// [Scrollbar.radius] property
  final Radius? radius;

  /// [Scrollbar.notificationPredicate] property
  final ScrollNotificationPredicate? notificationPredicate;

  /// [Scrollbar.interactive] property
  final bool? interactive;

  /// [Scrollbar.scrollbarOrientation] property
  final ScrollbarOrientation? scrollbarOrientation;

  /// Content child
  final Widget child;

  const ScrollableViewScrollbar({
    super.key,
    this.axisDirection = AxisDirection.down,
    required this.controller,
    this.physics,
    this.scrollBehavior,
    this.restorationId,
    this.thumbVisibility,
    this.trackVisibility,
    this.thickness,
    this.radius,
    this.notificationPredicate,
    this.interactive,
    this.scrollbarOrientation,
    required this.child,
  });

  factory ScrollableViewScrollbar.both({
    AxisDirection axisDirectionX = AxisDirection.right,
    AxisDirection axisDirectionY = AxisDirection.down,
    required ScrollController controllerX,
    required ScrollController controllerY,
    ScrollPhysics? physics,
    ScrollBehavior? scrollBehavior,
    String? restorationId,
    bool? thumbVisibility,
    bool? trackVisibility,
    double? thickness,
    Radius? radius,
    ScrollNotificationPredicate? notificationPredicate,
    bool? interactive,
    ScrollbarOrientation? scrollbarOrientation,
    required Widget child,
  }) =>
      ScrollableViewScrollbar(
          axisDirection: axisDirectionX,
          controller: controllerX,
          physics: physics,
          scrollBehavior: scrollBehavior,
          restorationId: restorationId,
          thumbVisibility: thumbVisibility,
          trackVisibility: trackVisibility,
          thickness: thickness,
          radius: radius,
          notificationPredicate: notificationPredicate,
          interactive: interactive,
          scrollbarOrientation: scrollbarOrientation,
          child: ScrollableViewScrollbar(
              axisDirection: axisDirectionY,
              controller: controllerY,
              physics: physics,
              scrollBehavior: scrollBehavior,
              restorationId: restorationId,
              thumbVisibility: thumbVisibility,
              trackVisibility: trackVisibility,
              thickness: thickness,
              radius: radius,
              notificationPredicate: notificationPredicate,
              interactive: interactive,
              scrollbarOrientation: scrollbarOrientation,
              child: child));

  @override
  State<ScrollableViewScrollbar> createState() => _ScrollableViewScrollbarState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(EnumProperty<AxisDirection>('axisDirection', axisDirection))
      ..add(DiagnosticsProperty<ScrollController>('controller', controller))
      ..add(DiagnosticsProperty<ScrollPhysics?>('physics', physics))
      ..add(DiagnosticsProperty<ScrollBehavior?>('scrollBehavior', scrollBehavior))
      ..add(StringProperty('restorationId', restorationId))
      ..add(DiagnosticsProperty<bool?>('thumbVisibility', thumbVisibility))
      ..add(DiagnosticsProperty<bool?>('trackVisibility', trackVisibility))
      ..add(DoubleProperty('thickness', thickness))
      ..add(DiagnosticsProperty<Radius?>('radius', radius))
      ..add(ObjectFlagProperty<ScrollNotificationPredicate?>.has('notificationPredicate', notificationPredicate))
      ..add(DiagnosticsProperty<bool?>('interactive', interactive))
      ..add(EnumProperty<ScrollbarOrientation?>('scrollbarOrientation', scrollbarOrientation));
  }
}

class _ScrollableViewScrollbarState extends State<ScrollableViewScrollbar>
    with TickerProviderStateMixin, RestorationMixin
    implements ScrollContext {
  final _RestorableScrollOffset _persistedScrollOffset = _RestorableScrollOffset();
  final GlobalKey _ignorePointerKey = GlobalKey();
  late ScrollBehavior _configuration;
  ScrollPosition? _position;
  bool _shouldIgnorePointer = false;

  @override
  String? get restorationId => widget.restorationId;

  @override
  BuildContext? get notificationContext => _ignorePointerKey.currentContext;

  @override
  BuildContext get storageContext => context;

  @override
  TickerProvider get vsync => this;

  @override
  AxisDirection get axisDirection => widget.axisDirection;

  @override
  double get devicePixelRatio => _devicePixelRatio;
  late double _devicePixelRatio;

  @override
  void didChangeDependencies() {
    _devicePixelRatio = MediaQuery.maybeDevicePixelRatioOf(context) ?? View.of(context).devicePixelRatio;
    _updatePosition();
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(ScrollableViewScrollbar oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.detach(_position!);
      widget.controller.attach(_position!);
    }

    bool shouldUpdatePosition() {
      var newPhysics = widget.physics ?? widget.scrollBehavior?.getScrollPhysics(context);
      var oldPhysics = oldWidget.physics ?? oldWidget.scrollBehavior?.getScrollPhysics(context);
      do {
        if (newPhysics?.runtimeType != oldPhysics?.runtimeType) return true;
        newPhysics = newPhysics?.parent;
        oldPhysics = oldPhysics?.parent;
      } while (newPhysics != null || oldPhysics != null);
      return widget.controller.runtimeType != oldWidget.controller.runtimeType;
    }

    if (shouldUpdatePosition()) _updatePosition();
  }

  @override
  Widget build(BuildContext context) => Scrollbar(
      controller: widget.controller,
      thumbVisibility: widget.thumbVisibility,
      trackVisibility: widget.trackVisibility,
      thickness: widget.thickness,
      radius: widget.radius,
      notificationPredicate: widget.notificationPredicate,
      interactive: widget.interactive,
      scrollbarOrientation: widget.scrollbarOrientation,
      child: _configuration.buildOverscrollIndicator(
          context,
          IgnorePointer(key: _ignorePointerKey, ignoring: _shouldIgnorePointer, child: widget.child),
          ScrollableDetails(direction: widget.axisDirection, controller: widget.controller)));

  @override
  void deactivate() {
    widget.controller.detach(_position!);
    super.deactivate();
  }

  @override
  void dispose() {
    _position!.dispose();
    _persistedScrollOffset.dispose();
    super.dispose();
  }

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(_persistedScrollOffset, 'offset');
    if (_persistedScrollOffset.value != null)
      _position?.restoreOffset(_persistedScrollOffset.value!, initialRestore: initialRestore);
  }

  @override
  void saveOffset(double offset) {
    _persistedScrollOffset.value = offset;
    ServicesBinding.instance.restorationManager.flushData();
  }

  @override
  void setCanDrag(bool value) {}

  @override
  void setIgnorePointer(bool value) {
    if (_shouldIgnorePointer == value) return;
    _shouldIgnorePointer = value;
    (_ignorePointerKey.currentContext?.findRenderObject() as RenderIgnorePointer?)?.ignoring = _shouldIgnorePointer;
  }

  @override
  void setSemanticsActions(Set<SemanticsAction> actions) {}

  void _updatePosition() {
    _configuration = widget.scrollBehavior ?? ScrollConfiguration.of(context);
    var physics = _configuration.getScrollPhysics(context);
    if (widget.physics != null) {
      physics = widget.physics!.applyTo(physics);
    } else if (widget.scrollBehavior != null) {
      physics = widget.scrollBehavior!.getScrollPhysics(context).applyTo(physics);
    }
    final oldPosition = _position;
    if (oldPosition != null) {
      if (widget.controller.positions.contains(oldPosition)) widget.controller.detach(oldPosition);
      scheduleMicrotask(oldPosition.dispose);
    }
    final position = _position = widget.controller.createScrollPosition(physics, this, oldPosition);
    if (!position.hasViewportDimension) position.applyViewportDimension(0);
    if (!position.hasContentDimensions) position.applyContentDimensions(0, 0);
    widget.controller.attach(position);
  }
}

class _RestorableScrollOffset extends RestorableValue<double?> {
  @override
  double? createDefaultValue() => null;

  @override
  void didUpdateValue(double? oldValue) => notifyListeners();

  @override
  double fromPrimitives(Object? data) => data as double;

  @override
  Object? toPrimitives() => value;

  @override
  bool get enabled => value != null;
}
