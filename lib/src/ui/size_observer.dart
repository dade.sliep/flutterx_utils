import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

/// Widget which reports child size changes to [onSizeChanged] callback
/// Useful if you want a parent widget to change behaviour depending on child size
/// The size returned by the callback is computed depending on [constraints]
class SizeObserver extends SingleChildRenderObjectWidget {
  /// If true getDryLayout will be used instead of layout to compute child size
  /// Using dry layout is more faster but some widget don't support it in that case consider using [SafeDryLayout]
  final bool useDryLayout;

  /// Size measurements will be based on this constraints parameter
  /// If null or if inner values are null, parent constraints will be used instead
  final PartialBoxConstraints? constraints;

  /// Callback for size change events
  final ValueChanged<Size> onSizeChanged;

  const SizeObserver({
    super.key,
    this.useDryLayout = true,
    this.constraints,
    required this.onSizeChanged,
    required Widget super.child,
  });

  @override
  RenderObject createRenderObject(BuildContext context) =>
      RenderSizeObserver._(useDryLayout, constraints, onSizeChanged);

  @override
  void updateRenderObject(BuildContext context, RenderSizeObserver renderObject) {
    if (renderObject._onSizeChanged != onSizeChanged)
      renderObject._oldSize = null;
    else if (renderObject._useDryLayout == useDryLayout && renderObject._constraints == constraints) return;
    renderObject
      .._useDryLayout = useDryLayout
      .._constraints = constraints
      .._onSizeChanged = onSizeChanged
      ..markNeedsLayout();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty<bool>('useDryLayout', useDryLayout))..add(DiagnosticsProperty<PartialBoxConstraints?>('constraints', constraints))..add(ObjectFlagProperty<ValueChanged<Size>>.has('onSizeChanged', onSizeChanged));
  }
}

class RenderSizeObserver extends RenderProxyBox {
  bool _useDryLayout;
  PartialBoxConstraints? _constraints;
  ValueChanged<Size> _onSizeChanged;

  // ignore: avoid_positional_boolean_parameters
  RenderSizeObserver._(this._useDryLayout, this._constraints, this._onSizeChanged);

  Size? _oldSize;

  @override
  void performLayout() {
    final constraints = this.constraints;
    final measureConstraints = _constraints?.resolve(constraints) ?? constraints;
    final Size size;
    if (measureConstraints != constraints) {
      if (_useDryLayout) {
        size = getDryLayout(measureConstraints);
      } else {
        child!.layout(measureConstraints, parentUsesSize: true);
        size = child!.size;
      }
      child!.layout(constraints, parentUsesSize: true);
      this.size = child!.size;
    } else {
      child!.layout(constraints, parentUsesSize: true);
      this.size = size = child!.size;
    }
    if (size != _oldSize) _onSizeChanged(_oldSize = size);
  }
}

/// Describe partial constraints. Null values will be resolved by [resolve] method
@immutable
class PartialBoxConstraints {
  final double? minWidth;
  final double? maxWidth;
  final double? minHeight;
  final double? maxHeight;

  const PartialBoxConstraints({
    this.minWidth,
    this.maxWidth,
    this.minHeight,
    this.maxHeight,
  });

  const PartialBoxConstraints.unconstrained({
    this.minWidth = 0.0,
    this.maxWidth = double.infinity,
    this.minHeight = 0.0,
    this.maxHeight = double.infinity,
  });

  BoxConstraints resolve(BoxConstraints parent) => BoxConstraints(
    minWidth: minWidth ?? parent.minWidth,
    maxWidth: maxWidth ?? parent.maxWidth,
    minHeight: minHeight ?? parent.minHeight,
    maxHeight: maxHeight ?? parent.maxHeight,
  );

  @override
  bool operator ==(Object other) =>
      other is PartialBoxConstraints &&
      other.minWidth == minWidth &&
      other.maxWidth == maxWidth &&
      other.minHeight == minHeight &&
      other.maxHeight == maxHeight;

  @override
  int get hashCode => Object.hash(minWidth, maxWidth, minHeight, maxHeight);
}

/// Some widget (e.g. InputDecorator) don't support dry layout
/// So build will fail if this kind of widgets are placed inside a [SizeObserver]
/// with [SizeObserver.useDryLayout] set to true
/// That's where you may consider wrapping your no-dry-layout widgets inside a [SafeDryLayout]
class SafeDryLayout extends SingleChildRenderObjectWidget {
  final Size? defaultSize;

  const SafeDryLayout({super.key, this.defaultSize, required super.child});

  @override
  RenderObject createRenderObject(BuildContext context) => RenderSafeDryLayout(defaultSize);

  @override
  void updateRenderObject(BuildContext context, RenderSafeDryLayout renderObject) {
    if (renderObject._defaultSize != defaultSize) renderObject._defaultSize = defaultSize;
    if (renderObject._size == null) renderObject.markNeedsLayout();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Size?>('defaultSize', defaultSize));
  }
}

class RenderSafeDryLayout extends RenderProxyBox {
  Size? _defaultSize;
  Size? _size;

  RenderSafeDryLayout(this._defaultSize);

  @override
  Size computeDryLayout(BoxConstraints constraints) => _size ?? _defaultSize ?? constraints.smallest;

  @override
  void performLayout() {
    super.performLayout();
    final size = this.size;
    if (size != _size) {
      _size = size;
      scheduleMicrotask(markNeedsLayoutForSizedByParentChange);
    }
  }
}
