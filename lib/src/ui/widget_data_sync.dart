import 'package:flutter/foundation.dart';

class WidgetDataSync<T> {
  final ValueGetter<T>? fallback;
  final ValueGetter<T?> provide;
  final ValueChanged<T>? register;
  final ValueChanged<T>? unregister;
  late final T _fallback = fallback?.call() as T;
  late T data;

  WidgetDataSync({
    this.fallback,
    required this.provide,
    this.register,
    this.unregister,
  }) {
    data = provide() ?? _fallback;
    register?.call(data);
  }

  void sync() {
    final oldData = data;
    final newDatA = provide() ?? _fallback;
    if (oldData != newDatA) {
      data = newDatA;
      unregister?.call(oldData);
      register?.call(newDatA);
    }
  }

  void dispose() => unregister?.call(data);
}
