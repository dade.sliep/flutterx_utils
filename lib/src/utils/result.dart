import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutterx_utils/src/utils/loggable.dart';

/// A discriminated union that encapsulates a successful outcome with a value of type [T]
/// or a failure with an arbitrary [Failure] exception.
@immutable
class Result<T> implements Printable {
  final dynamic _value;

  /// returns an instance that encapsulates the given [_value] as successful value.
  const Result.success(T value) : _value = value;

  /// returns an instance that encapsulates the given [error] and [stack] as [Failure].
  Result.failure(Object error, [StackTrace? stack]) : _value = Failure._(error, stack);

  /// Create instance of Result from function
  static Result<T> wrap<T>(ValueGetter<T> function) {
    try {
      return Result.success(function());
    } catch (error, stack) {
      return Result.failure(error, stack);
    }
  }

  /// Create instance of Result from async function
  static Future<Result<T>> wrapAsync<T>(ValueGetter<FutureOr<T>> function) {
    try {
      final result = function();
      return result is Future<T>
          ? result.then(Result.success, onError: Result.failure)
          : SynchronousFuture(Result.success(result));
    } catch (error, stack) {
      return SynchronousFuture(Result.failure(error, stack));
    }
  }

  /// returns `true` if this instance represents a successful outcome.
  /// In this case [isFailure] returns `false`.
  bool get isSuccess => _value is! Failure;

  /// returns `true` if this instance represents a failed outcome.
  /// In this case [isSuccess] returns `false`.
  bool get isFailure => _value is Failure;

  /// returns the encapsulated value if this instance represents [isSuccess] or call [Failure.throwError]
  /// of the encapsulated [Failure]
  T get resultOrThrow => _value is Failure ? (_value as Failure).throwError() : _value;

  /// returns the encapsulated [Failure] if this instance represents [isFailure] or null
  Failure? get errorOrNull => _value is Failure ? _value : null;

  /// returns the result of [onSuccess] for the encapsulated value if this instance represents [isSuccess]
  /// or the result of [onError] function for the encapsulated [Failure] exception if it is [isFailure].
  R handle<R>({R Function(T value)? onSuccess, R Function(Failure error)? onError}) => _value is Failure
      ? (onError == null ? (_value as Failure).throwError() : onError(_value))
      : (onSuccess == null ? _value : onSuccess(_value));

  /// Calls [onSuccess] with the encapsulated value if this instance represents [isSuccess]
  void onSuccess(void Function(T value) onSuccess) => isSuccess ? onSuccess(_value) : null;

  /// Calls [onError] with the encapsulated [Failure] exception if this instance represents [isFailure]
  void onError(void Function(Failure error) onError) => isFailure ? onError(_value) : null;

  @override
  void print(Loggable _) => _.logInfo('Result(${isSuccess ? 'success' : 'failure'})', () => _.logInfo(_value));

  @override
  bool operator ==(Object other) => identical(this, other) || other is Result && _value == other._value;

  @override
  int get hashCode => Object.hash(runtimeType, _value);

  @override
  String toString() => isSuccess ? 'Result.success($_value)' : 'Result.failure($_value)';
}

/// Failure representation of [Result] outcome
@immutable
class Failure implements Printable {
  /// The error can be either an [Error] or an [Exception]
  final Object error;

  /// The stack of the error
  final StackTrace? stack;

  const Failure._(this.error, this.stack);

  /// Throw the [error] enclosed by this Failure
  Never throwError() => throw error; // ignore: only_throw_errors

  @override
  void print(Loggable _) => _.logError('Failure', error, stack);

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Failure && error == other.error && stack == other.stack;

  @override
  int get hashCode => Object.hash(error, stack);

  @override
  String toString() => 'Failure($error)';
}
