typedef AnyFunctionCallback<R> = R Function(List<Type> typeArgs, List positionalArgs, Map<Symbol, dynamic> namedArgs);

/// Callable object that delegates invocation to [callback] method
///
/// ```dart
/// final f = AnyFunction.create<String>(
///     (typeArgs, positionalArgs, namedArgs) => '${positionalArgs[0]}${namedArgs[#a] + namedArgs[#b]}');
/// print(f('a + b = ', a: 1, b: 2));
/// ```
class AnyFunction<R> {
  final AnyFunctionCallback<R> callback;

  const AnyFunction._(this.callback);

  static dynamic create<R>(AnyFunctionCallback<R> callback) => AnyFunction<R>._(callback);

  R call() => callback(const <Type>[], const [], const <Symbol, dynamic>{});

  @override
  R noSuchMethod(Invocation invocation) => invocation.isMethod
      ? callback(invocation.typeArguments, invocation.positionalArguments, invocation.namedArguments)
      : super.noSuchMethod(invocation);
}
