import 'package:flutter/foundation.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

/// JsonPath represents a path of keys inside a json like collection
/// The path is composed only by nested [Map] and [List] objects except the last segment that can have any type of value
/// Each path entry has a [parent] which is the parent entry or [root] for the first segment
/// And a [value] which is the key of the current path entry (any type for maps and int index for lists)
/// The [root] element is is the element that does not have a [parent]
@immutable
class JsonPath implements Comparable<JsonPath> {
  static const JsonPath root = JsonPath([]);

  JsonPath? get parent {
    final l = segments.length - 1;
    return l < 0 ? null : JsonPath([...segments.take(l)]);
  }

  dynamic get value => segments.isEmpty ? null : segments.last;

  bool get isRoot => segments.isEmpty;

  List get segmentsToRoot => List.unmodifiable(segments.reversed);

  final List segments;

  const JsonPath(this.segments);

  JsonPath.value(dynamic value) : this([value]);

  JsonPath append(dynamic value) => JsonPath([...segments, value]);

  bool containsPath(JsonPath other) {
    final ia = segments.iterator;
    final ib = other.segments.iterator;
    while (ia.moveNext()) {
      if (!ib.moveNext()) return false;
      if (ia.current != ib.current) return false;
    }
    return true;
  }

  bool contains(Object target) {
    final t = requireNotNull(parent).get(target);
    final key = segments.last;
    if (t is Map) {
      return t.containsKey(key);
    } else if (t is List) {
      final k = key is int ? key : int.parse(key);
      return k >= 0 && k < t.length;
    } else {
      _error('query', target);
    }
  }

  /// Get element of [target] at this path
  E get<E>(Object target) {
    var t = target;
    for (final key in segments) {
      if (t is Map) {
        t = t[key];
      } else if (t is List) {
        t = t[key is int ? key : int.parse(key)];
      } else {
        _error('query', target);
      }
    }
    return t as E;
  }

  /// Remove element of [target] at this path
  E remove<E>(Object target) {
    final t = requireNotNull(parent).get(target);
    final key = segments.last;
    if (t is Map) {
      return t.remove(key);
    } else if (t is List) {
      return t.removeAt(key is int ? key : int.parse(key));
    } else {
      _error('remove', target);
    }
  }

  /// Set element of [target] at this path to [data]
  void set(Object target, dynamic data) {
    final t = requireNotNull(parent).get(target);
    final key = segments.last;
    if (t is Map) {
      t[key] = data;
    } else if (t is List) {
      t[key is int ? key : int.parse(key)] = data;
    } else {
      _error('set', target);
    }
  }

  @override
  String toString() => segments.join('.');

  @override
  int get hashCode => Object.hashAll(segments);

  @override
  bool operator ==(Object other) => identical(other, this) || other is JsonPath && listEquals(segments, other.segments);

  @override
  int compareTo(JsonPath other) => toString().compareTo(other.toString());

  /// throws UnsupportedError if [target] is not either a [Map] or a [List]
  Never _error(String action, Object target) =>
      throw UnsupportedError('Cannot perform action \'$action\' on path \'$this\' of ${target.runtimeType} $target');
}

extension JsonExt on JsonObject {
  E xget<E>(dynamic path, [E? fallback]) {
    final p = _path(path);
    try {
      return p.get(this);
    } catch (_) {
      if (fallback is E) return fallback;
      rethrow;
    }
  }

  void xset(dynamic path, dynamic value) => _path(path).set(this, value);

  E xremove<E>(dynamic path) => _path(path).remove<E>(this);

  bool xcontains(dynamic path) => _path(path).contains(this);

  JsonPath _path(dynamic path) {
    if (path is String) return JsonPath(path.split('.'));
    if (path is List) return JsonPath(path);
    if (path is Iterable) return JsonPath([...path]);
    if (path is JsonPath) return path;
    throw ArgumentError.value(path, 'path', 'must be of type String|Iterable|JsonPath');
  }
}
