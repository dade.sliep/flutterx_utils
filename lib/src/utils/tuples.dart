import 'package:flutter/foundation.dart';

/// Represents a generic pair of two values.
///
/// There is no meaning attached to values in this class, it can be used for any purpose.
/// Pair exhibits value semantics, i.e. two pairs are equal if both components are equal.
@immutable
class Pair<A, B> {
  /// First value
  final A first;

  /// Second value
  final B second;

  /// Creates a new instance of Pair.
  const Pair(this.first, this.second);

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Pair && first == other.first && second == other.second;

  @override
  int get hashCode => Object.hash(first, second);

  @override
  String toString() => 'Pair($first, $second)';
}

/// Represents a triad of values
///
/// There is no meaning attached to values in this class, it can be used for any purpose.
/// Triple exhibits value semantics, i.e. two triples are equal if all three components are equal.
@immutable
class Triple<A, B, C> {
  /// First value
  final A first;

  /// Second value
  final B second;

  /// Third value
  final C third;

  /// Creates a new instance of Triple.
  const Triple(this.first, this.second, this.third);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Triple && first == other.first && second == other.second && third == other.third;

  @override
  int get hashCode => Object.hash(first, second, third);

  @override
  String toString() => 'Triple($first, $second, $third)';
}
