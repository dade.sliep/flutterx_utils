import 'package:flutter/material.dart';

/// Utility class for [MediaQueryData]
/// [of] same as [MediaQuery.of] but returns the [MediaQueryData] of the root [MediaQuery] ancestor of given context
class RootMediaQuery {
  const RootMediaQuery._();

  /// @see [MediaQuery.of]
  /// The data from the farthest instance of this class that encloses the given
  /// context.
  static MediaQueryData of(BuildContext context) {
    late MediaQuery result;
    context.visitAncestorElements((element) {
      final widget = element.widget;
      if (widget is MediaQuery) result = widget;
      return true;
    });
    return result.data;
  }
}
