import 'package:flutterx_utils/flutterx_utils.dart';
import 'package:flutterx_utils/src/extensions/bool.dart';

/// Type alias for json object map
typedef JsonObject = Map<String, dynamic>;

/// Converts a [JsonObject] into [T]
typedef FromJsonObject<T> = T Function(JsonObject json);

/// Converts a [List] into [T]
typedef FromJsonList<T> = T Function(List json);

/// Converts a json element into [T]
typedef FromJson<T> = T Function(dynamic json);

/// Mixin for Data Transfer Objects
/// Every subclass of this mixin must also provide fromJson static method:
/// ```dart
/// static Foo fromJson(JsonObject json) => Foo(name: json['name'], age: json['age']);
/// ```
mixin DTO {
  /// Converts this instance into a json object map. E.g. JsonObject toJson() => {'name': name, 'age': age};
  JsonObject toJson();

  /// Default toString implementation: E.g. Foo(name: Ugo, age: 37)
  @override
  String toString() {
    var optimizedValue = 'DTO';
    assert(() {
      optimizedValue = '$runtimeType(${toJson().entries.map((entry) => '${entry.key}: ${entry.value}').join(', ')})';
      return true;
    }(), '');
    return optimizedValue;
  }
}

bool fromJsonBool(Object value) => value is bool ? value : parseBool(value.toString());

int fromJsonInt(Object value) => value is num ? value.toInt() : double.parse(value.toString()).toInt();

double fromJsonDouble(Object value) => value is num ? value.toDouble() : double.parse(value.toString());

String fromJsonString(Object value) => value.toString();

E fromJsonEnum<E>(String name, Map<String, E> values) =>
    values[name] ??
    (throw ArgumentError.value(
        name, E.toString(), 'has no match with any of available values: [${values.keys.join(', ')}]'));

List<E> fromJsonList<E>(List json, {FromJson<E>? fromJson}) => fromJson == null
    ? json is List<E>
        ? json
        : json.cast<E>()
    : json.map<E>(fromJson).toList();

Map<K, V> fromJsonMap<K, V>(Map json, {FromJson<K>? fromJsonKey, FromJson<V>? fromJsonValue}) => fromJsonKey == null
    ? fromJsonValue == null
        ? json is Map<K, V>
            ? json
            : json.cast<K, V>()
        : json.map<K, V>((key, value) => MapEntry(key, fromJsonValue(value)))
    : fromJsonValue == null
        ? json.map<K, V>((key, value) => MapEntry(fromJsonKey(key), value))
        : json.map<K, V>((key, value) => MapEntry(fromJsonKey(key), fromJsonValue(value)));

extension DTOListExt<T extends DTO> on Iterable<T> {
  List<JsonObject> toJsonList() => map<JsonObject>((item) => item.toJson()).toList();
}
