import 'dart:async';

/// Token for cancelling a pending operation.
///
/// - create your token (final token = CancellationToken())
/// - attach to a pending task future (myTask = token.attach(myTask))
/// - cancel in any moment (token.cancel('just changed my mind'))
///
/// Alternatively you can just subscribe to [onCancel] (token.onCancel(...))
class CancellationToken {
  Completer? _completer;

  /// Check if there is a pending operation attached. When this is false, [cancel] will do nothing
  bool get active => _completer != null;

  /// Create a cancellation token
  CancellationToken();

  /// Create a cancellation token that will automatically cancel after given [duration] from when [attach] is called
  factory CancellationToken.timeout(Duration duration) => _TimeOutCancellationToken(duration);

  /// Wrap [task] with this token. (alternative to [onCancel])
  /// After this call you should use the returned future instead of [task]
  /// The returned future will raise a [CancelException] when cancellation is requested
  Future<T> attach<T>(Future<T> task) {
    assert(_completer == null, 'already attached to another future');
    final completer = _completer = Completer<T>();
    task.then(completer.complete, onError: completer.completeError);
    return completer.future..whenComplete(() => _completer = null);
  }

  /// Subscribe to [cancel] event (alternative to [attach])
  void onCancel(void Function(CancelException error, StackTrace stackTrace) onCancel) {
    assert(_completer == null, 'already subscribed to onCancel');
    _completer = Completer<void>()
      ..future.onError<CancelException>((error, stackTrace) {
        onCancel(error, stackTrace);
        _completer = null;
      });
  }

  /// Cancel pending operation
  void cancel([String message = 'operation canceled']) =>
      _completer?.completeError(CancelException._(message), StackTrace.current);
}

/// Exception raised when a cancellation is requested
class CancelException implements Exception {
  final String message;

  const CancelException._(this.message);

  @override
  String toString() => message;
}

class _TimeOutCancellationToken extends CancellationToken {
  final Duration duration;

  _TimeOutCancellationToken(this.duration);

  @override
  Future<T> attach<T>(Future<T> future) =>
      super.attach(future)..whenComplete(Timer(duration, () => cancel('operation canceled after $duration')).cancel);
}
