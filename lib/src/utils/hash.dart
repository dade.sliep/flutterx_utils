int hashMap(Map<Object?, Object?> object) {
  var sum = 0;
  var count = 0;
  const mask = 0x3FFFFFFF;
  for (final entry in object.entries) {
    sum = (sum + _smear(entry.key.hashCode)) & mask;
    sum = (sum + _smear(entry.value.hashCode)) & mask;
    count += 1;
  }
  var hash = 0;
  hash = _combine(hash, sum);
  hash = _combine(hash, count);
  return _finish(hash);
}

int _smear(int x) {
  x ^= x >>> 16;
  x = (x * 0x7feb352d) & 0xFFFFFFFF;
  x ^= x >>> 15;
  x = (x * 0x846ca68b) & 0xFFFFFFFF;
  x ^= x >>> 16;
  return x;
}

int _combine(int hash, int value) {
  hash = 0x1fffffff & (hash + value);
  hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
  return hash ^ (hash >> 6);
}

int _finish(int hash) {
  hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
  hash = hash ^ (hash >> 11);
  return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
}
