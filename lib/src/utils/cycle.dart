/// An iterable allows to recursively cycle a field of [T] from [root] while the result is not null
class Cycle<T extends Object> extends Iterable<T> {
  /// The first element of the iterable
  final T root;

  /// Get the next element from current
  final T? Function(T current) cycle;

  /// e.g. Cycle(node, cycle: (node) => node.parent)
  const Cycle(this.root, {required this.cycle});

  @override
  Iterator<T> get iterator => _CycleIterator<T>._(this);
}

class _CycleIterator<T extends Object> extends Iterator<T> {
  final Cycle<T> _iterable;
  bool _first = true;

  _CycleIterator._(this._iterable);

  @override
  late T current;

  @override
  bool moveNext() {
    if (_first) {
      current = _iterable.root;
      _first = false;
      return true;
    }
    final parent = _iterable.cycle(current);
    if (parent == null) return false;
    current = parent;
    return true;
  }
}
