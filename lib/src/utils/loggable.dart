import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterx_utils/flutterx_utils.dart';
import 'package:flutterx_utils/src/extensions/code.dart';

/// Logging utility
mixin Loggable {
  static const LogLevel _defaultLogLevel = kReleaseMode ? LogLevel.none : LogLevel.debug;
  static final Map<String, LogLevel> _levelByTag = {};
  String? _tag;

  /// [Indent] of the log
  final Indent indent = Indent(2);

  /// Log tag which is the runtime type of who implements this mixin by default
  String get tag => _tag ??= tagOf(this);

  /// Log level
  LogLevel get level => _levelByTag[tag] ?? _defaultLogLevel;

  void logDebug(Object message, [VoidCallback? details]) => logInternal(LogLevel.debug, message, details: details);

  void logInfo(Object message, [VoidCallback? details]) => logInternal(LogLevel.info, message, details: details);

  void logWarning(Object message, [VoidCallback? details]) => logInternal(LogLevel.warning, message, details: details);

  void logError(Object message, [Object? error, StackTrace? stack, VoidCallback? details]) =>
      logInternal(LogLevel.error, message, error: error, stack: stack, details: details);

  void log(LogLevel level, Object message, {Object? error, StackTrace? stack, VoidCallback? details}) =>
      logInternal(level, message, error: error, stack: stack, details: details);

  void logRun(VoidCallback run) {
    if (level.index < LogLevel.none.index)
      try {
        indent.increase();
        run();
      } finally {
        indent.decrease();
      }
  }

  /// Actual log implementation, override this method to handle log write
  @protected
  void logInternal(LogLevel level, Object message, {Object? error, StackTrace? stack, VoidCallback? details}) {
    if (level.index < this.level.index) return;
    if (message is Printable) return message.print(this);
    final prefix = '[$tag:${level.name.toUpperCase()}] $indent';
    for (final line in LineSplitter.split(message.toString())) debugPrint('$prefix$indent$line');
    if (error != null) logInternal(level, error, details: stack == null ? null : () => logInternal(level, stack));
    details?.let(logRun);
  }

  static void setLevelFor<T extends Loggable>(LogLevel level) => setLevelForTag(T.toString(), level);

  static void setLevelForTag(String tag, LogLevel level) => _levelByTag[tag] = level;

  static String tagOf(Object object) => object is State
      ? objectRuntimeType(object.widget, object.widget.toStringShort())
      : objectRuntimeType(object, object.toString());
}

enum LogLevel { debug, info, warning, error, none }

/// Utility class that helps to provide indent spacing
/// Just through [toString] you can get the indentation of current level
class Indent {
  int _level = 0;

  /// The number of spaces per indent level
  final int step;

  /// The current indent level can be modified by invoking [increase] and [decrease]
  int get level => _level;

  /// Create Indent instance with given [step]
  Indent(this.step);

  /// Get indent by [level]
  String operator [](int level) => ' ' * (step * level);

  /// Increase level
  void increase() => _level++;

  /// Decrease level
  void decrease() => _level--;

  @override
  String toString() => this[_level];
}

/// Implement this interface to provide specific logging for an object
/// [print] is called when you pass this instance as message argument of [Loggable.log] e.g. log(myPrintable)
abstract class Printable {
  const Printable();

  /// Describe this instance using [_] [Loggable]
  /// Waring: calling _.log(this) will result in a stack overflow
  @protected
  void print(Loggable _);
}
