import 'dart:async';

/// A [Future] that never returns
/// Be careful using this class: it may causes some resources to never being released
class NeverFuture<T> implements Future<T> {
  const NeverFuture();

  @override
  Stream<T> asStream() => Stream<T>.fromFuture(this);

  @override
  Future<T> catchError(Function onError, {bool Function(Object error)? test}) => NeverFuture<T>();

  @override
  Future<R> then<R>(FutureOr<R> Function(T value) onValue, {Function? onError}) => NeverFuture<R>();

  @override
  Future<T> timeout(Duration timeLimit, {FutureOr<T> Function()? onTimeout}) {
    final completer = Completer<T>();
    if (onTimeout == null) {
      Timer(timeLimit,
          () => completer.completeError(TimeoutException('Future not completed', timeLimit), StackTrace.empty));
    } else {
      final zone = Zone.current;
      final onTimeoutHandler = zone.registerCallback(onTimeout);
      Timer(timeLimit, () {
        try {
          completer.complete(zone.run(onTimeoutHandler));
        } catch (e, s) {
          completer.completeError(e, s);
        }
      });
    }
    return completer.future;
  }

  @override
  Future<T> whenComplete(FutureOr<dynamic> Function() action) => NeverFuture<T>();
}
