import 'dart:async';

import 'package:flutter/foundation.dart';

/// Mutual exclusion.
/// Acquire and release lock
class Mutex {
  Completer<void>? _completer;

  /// Check if mutex is currently acquired
  bool get acquired => _completer != null;

  /// acquire while function is executing then release
  Future<T> lock<T>(Future<T> Function() function) => acquire().then((_) => function().whenComplete(release));

  Future<void> acquire() {
    final c = _completer;
    if (c == null) {
      _completer = Completer();
      return SynchronousFuture(null);
    } else {
      return c.future.then((_) => acquire());
    }
  }

  void release() {
    assert(_completer != null, 'cannot release, nothing acquired');
    _completer!.complete();
    _completer = null;
  }
}

/// Helper for running tasks with mutex
abstract class MutexRun {
  final Mutex mutex;

  MutexRun(this.mutex);

  /// Run task exactly once, after first execution every other call will be ignored
  factory MutexRun.once([Mutex? mutex]) => _RunOnce(mutex ?? Mutex());

  /// Run task, every call made while another call is running will be ignored until the first one completes
  factory MutexRun.first([Mutex? mutex]) => _RunFirst(mutex ?? Mutex());

  /// Run task once every [delay], after the first call other call will be ignored until delay passed.
  factory MutexRun.every(Duration delay, [Mutex? mutex]) => _RunEvery(delay, mutex ?? Mutex());

  /// Run task after [delay], every call will invalidate the previous one and only the last call within the delay
  /// will be performed
  factory MutexRun.last(Duration delay, [Mutex? mutex]) => _RunLast(delay, mutex ?? Mutex());

  /// Execute async task (depending on rules) and return true if [function] has been executed
  Future<T> call<T>(Future<T> Function() function, {FutureOr<T>? fallback = const ConcurrentExecutionError()});

  /// Reset to initial state
  void invalidate() {}

  static Future<T> _fallback<T>(FutureOr<T>? fallback) =>
      fallback is Future<T> ? fallback : SynchronousFuture(fallback as T);
}

class _RunOnce extends MutexRun {
  bool _done = false;

  _RunOnce(super.mutex);

  @override
  Future<T> call<T>(Future<T> Function() function, {FutureOr<T>? fallback = const ConcurrentExecutionError()}) {
    assert(fallback is T || fallback is Future<T>, 'fallback must be of type $T');
    return _done
        ? MutexRun._fallback(fallback)
        : mutex.lock<T>(() => _done
            ? MutexRun._fallback(fallback)
            : function().then((result) {
                _done = true;
                return result;
              }));
  }

  @override
  void invalidate() => _done = false;
}

class _RunFirst extends MutexRun {
  bool _running = false;

  _RunFirst(super.mutex);

  @override
  Future<T> call<T>(Future<T> Function() function, {FutureOr<T>? fallback = const ConcurrentExecutionError()}) {
    assert(fallback is T || fallback is Future<T>, 'fallback must be of type $T');
    return _running
        ? MutexRun._fallback(fallback)
        : mutex.lock<T>(() {
            _running = true;
            return function().whenComplete(() => _running = false);
          });
  }

  @override
  void invalidate() => _running = false;
}

class _RunEvery extends MutexRun {
  final Duration delay;
  bool _running = false;
  DateTime? _lastExecution;

  _RunEvery(this.delay, Mutex mutex)
      : assert(delay > Duration.zero, 'Use MutexRun.first instead'),
        super(mutex);

  @override
  Future<T> call<T>(Future<T> Function() function, {FutureOr<T>? fallback = const ConcurrentExecutionError()}) {
    assert(fallback is T || fallback is Future<T>, 'fallback must be of type $T');
    return _running || (_lastExecution?.isAfter(DateTime.now().subtract(delay)) ?? false)
        ? MutexRun._fallback(fallback)
        : mutex.lock<T>(() {
            _running = true;
            return function().then((result) {
              _lastExecution = DateTime.now();
              return result;
            }).whenComplete(() => _running = false);
          });
  }

  @override
  void invalidate() {
    _running = false;
    _lastExecution = null;
  }
}

class _RunLast extends MutexRun {
  final Duration delay;
  bool _running = false;
  DateTime? _lastCall;

  _RunLast(this.delay, Mutex mutex)
      : assert(delay > Duration.zero, 'Use MutexRun.first instead'),
        super(mutex);

  @override
  Future<T> call<T>(Future<T> Function() function, {FutureOr<T>? fallback = const ConcurrentExecutionError()}) {
    assert(fallback is T || fallback is Future<T>, 'fallback must be of type $T');
    final now = _lastCall = DateTime.now();
    return Future.delayed(
        delay,
        () => _running || now != _lastCall
            ? MutexRun._fallback(fallback)
            : mutex.lock<T>(() {
                _running = true;
                return function().whenComplete(() => _running = false);
              }));
  }

  @override
  void invalidate() {
    _running = false;
    _lastCall = null;
  }
}

class ConcurrentExecutionError<T> implements Future<T> {
  final String message;

  Future<T> get _delegate => Future<T>.error(StateError(message));

  const ConcurrentExecutionError({this.message = 'concurrent execution'});

  @override
  Stream<T> asStream() => _delegate.asStream();

  @override
  Future<T> catchError(Function onError, {bool Function(Object error)? test}) =>
      _delegate.catchError(onError, test: test);

  @override
  Future<R> then<R>(FutureOr<R> Function(T value) onValue, {Function? onError}) =>
      _delegate.then<R>(onValue, onError: onError);

  @override
  Future<T> timeout(Duration timeLimit, {FutureOr<T> Function()? onTimeout}) =>
      _delegate.timeout(timeLimit, onTimeout: onTimeout);

  @override
  Future<T> whenComplete(FutureOr<void> Function() action) => _delegate.whenComplete(action);
}
