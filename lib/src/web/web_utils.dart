/// Web utils
library web_utils;

export 'package:flutterx_utils/src/web/web_utils_io.dart'
    if (dart.library.html) 'package:flutterx_utils/src/web/web_utils_html.dart';
