Future<void> webOpenRedirect(Uri uri) => throw UnimplementedError('unimplemented for this platform');

Future<void> webOpenNewTab(Uri uri) => throw UnimplementedError('unimplemented for this platform');

Uri get webHref => throw UnimplementedError('unimplemented for this platform');

set webHref(Uri value) => throw UnimplementedError('unimplemented for this platform');

Future<void> webReload() => throw UnimplementedError('unimplemented for this platform');
