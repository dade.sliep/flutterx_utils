// ignore_for_file: unsafe_html
import 'dart:async';
import 'dart:html'; // ignore: avoid_web_libraries_in_flutter

import 'package:flutterx_utils/flutterx_utils.dart';

Future<void> webOpenRedirect(Uri uri) {
  window.open(uri.toString(), '_self');
  return const NeverFuture();
}

Future<void> webOpenNewTab(Uri uri) {
  window.open(uri.toString(), '_blank');
  return Future.value();
}

Uri get webHref => Uri.parse(window.location.href);

set webHref(Uri value) => window.location.href = value.toString();

Future<void> webReload() {
  window.location.reload();
  return const NeverFuture();
}
