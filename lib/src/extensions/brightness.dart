import 'dart:ui';

extension BrightnessExt on Brightness {
  /// Check if this is equal to [Brightness.dark]
  bool get isDark => this == Brightness.dark;

  /// Get negative brightness value
  Brightness get negative => isDark ? Brightness.light : Brightness.dark;
}
