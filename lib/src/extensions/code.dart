import 'package:flutter/foundation.dart';

extension HighOrderFunctions<T> on T {
  /// Useful to use with nullable variables e.g. value?.let(doSomething);
  R let<R>(R Function(T value) let) => let(this);
}

/// If [value] is not null, returns value. Else throws a [StateError] with custom [message]
T requireNotNull<T>(T? value, {ValueGetter<String>? message}) =>
    value ?? (throw StateError(message?.call() ?? 'Required value was null.'));

/// Repeat [count] times some [computation]
void repeat(int count, void Function(int i) computation) {
  for (var i = 0; i < count; i++) computation(i);
}
