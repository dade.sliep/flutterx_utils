extension StringExt on String {
  static final _camelCaseMatcher = RegExp('[A-Z][a-z]*');

  /// From 'foo_bar' to 'fooBar'
  String get lowerCamelCase {
    final out = StringBuffer();
    final parts = split('_');
    for (var i = 0; i < parts.length; i++) {
      final part = parts[i];
      if (part.isNotEmpty) out.write(i == 0 ? part.toLowerCase() : part.capitalized);
    }
    return out.toString();
  }

  /// from 'foo_bar' to 'FooBar'
  String get upperCamelCase {
    final out = StringBuffer();
    final parts = split('_');
    for (var i = 0; i < parts.length; i++) {
      final part = parts[i];
      if (part.isNotEmpty) out.write(part.capitalized);
    }
    return out.toString();
  }

  /// from 'foo' to 'Foo'
  String get capitalized => isEmpty ? this : this[0].toUpperCase() + substring(1);

  /// Return null if empty
  String? get nonEmptyOrNull => isEmpty ? null : this;

  /// from fooBar to foo_bar
  String get snakeCase =>
      replaceAllMapped(_camelCaseMatcher, (match) => '${match.start == 0 ? '' : '_'}${match[0]!.toLowerCase()}');

  /// Trim start margin of multiline string literal by [depth] (e.g. '''    hello'''.trimMargin() -> 'hello')
  /// [depth] is the count of spaces (' ') before the actual start of the line
  String trimMargin([int depth = 4]) {
    final margin = ' ' * depth;
    return replaceFirst(margin, '').replaceAll('\n$margin', '\n');
  }
}
