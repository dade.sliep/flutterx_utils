import 'dart:async';

extension FutureExt<T> on Future<T> {
  /// Create a Future which is the union of this and [other]
  /// The result is the first which completes
  Future<T> operator |(Future<T> other) {
    final completer = Completer<T>();
    void complete(T value) => completer.isCompleted ? null : completer.complete(value);
    void completeError(Object error, StackTrace? stackTrace) =>
        completer.isCompleted ? null : completer.completeError(error, stackTrace);
    then(complete, onError: completeError);
    other.then(complete, onError: completeError);
    return completer.future;
  }
}
