import 'package:flutterx_utils/src/extensions/list.dart';
import 'package:flutterx_utils/src/utils/dto.dart';

extension MapExt<K, V> on Map<K, V> {
  /// Get all values where key satisfies [test] predicate
  Iterable<V> valuesWhereKey(bool Function(K key) test) sync* {
    for (final key in keys) if (test(key)) yield this[key]!;
  }

  /// Get all keys where value satisfies [test] predicate
  Iterable<K> keysWhereValue(bool Function(V value) test) sync* {
    for (final key in keys) if (test(this[key] as V)) yield key;
  }

  bool requireBool(String key) => require<Object, bool>(key, fromJsonBool);

  bool getBool(String key, {bool fallback = false}) => opt<Object, bool>(key, fromJsonBool) ?? fallback;

  bool? optBool(String key) => opt<Object, bool>(key, fromJsonBool);

  int requireInt(String key) => require<Object, int>(key, fromJsonInt);

  int getInt(String key, {int fallback = 0}) => opt<Object, int>(key, fromJsonInt) ?? fallback;

  int? optInt(String key) => opt<Object, int>(key, fromJsonInt);

  double requireDouble(String key) => require<Object, double>(key, fromJsonDouble);

  double getDouble(String key, {double fallback = 0}) => opt<Object, double>(key, fromJsonDouble) ?? fallback;

  double? optDouble(String key) => opt<Object, double>(key, fromJsonDouble);

  String requireString(String key) => require<Object, String>(key, fromJsonString);

  String getString(String key, {String fallback = ''}) => opt<Object, String>(key, fromJsonString) ?? fallback;

  String? optString(String key) => opt<Object, String>(key, fromJsonString);

  E requireEnum<E extends Enum>(String key, List<E> values, {bool ignoreCase = false, bool index = false}) => index
      ? require<int, E>(key, (i) => values[i])
      : require<String, E>(key, (name) => values.requireFromName(name, ignoreCase: ignoreCase));

  E getEnum<E extends Enum>(String key, List<E> values,
          {bool ignoreCase = false, bool index = false, required E fallback}) =>
      optEnum<E>(key, values, ignoreCase: ignoreCase, index: index) ?? fallback;

  E? optEnum<E extends Enum>(String key, List<E> values, {bool ignoreCase = false, bool index = false}) => index
      ? opt<int, E>(key, (i) => values[i])
      : opt<String, E>(key, (name) => values.requireFromName(name, ignoreCase: ignoreCase));

  E requireEnumMap<E>(String key, Map<String, E> values, {bool lower = false}) =>
      require<String, E>(key, (name) => fromJsonEnum(lower ? name.toLowerCase() : name, values));

  E getEnumMap<E>(String key, Map<String, E> values, {bool lower = false, required E fallback}) =>
      opt<String, E>(key, (name) => fromJsonEnum(lower ? name.toLowerCase() : name, values)) ?? fallback;

  E? optEnumMap<E>(String key, Map<String, E> values, {bool lower = false}) =>
      opt<String, E>(key, (name) => fromJsonEnum(lower ? name.toLowerCase() : name, values));

  E requireJsonObject<E>(String key, FromJsonObject<E> fromJson) => require<JsonObject, E>(key, fromJson);

  E? optJsonObject<E>(String key, FromJsonObject<E> fromJson) => opt<JsonObject, E>(key, fromJson);

  List<E> requireJsonList<E>(String key, FromJsonObject<E> fromJson) =>
      require<List, List<E>>(key, (json) => fromJsonList(json, fromJson: (json) => fromJson(json)));

  List<E> getJsonList<E>(String key, FromJsonObject<E> fromJson, {List<E>? fallback}) =>
      opt<List, List<E>>(key, (json) => fromJsonList(json, fromJson: (json) => fromJson(json))) ?? fallback ?? <E>[];

  List<E>? optJsonList<E>(String key, FromJsonObject<E> fromJson) =>
      opt<List, List<E>>(key, (json) => fromJsonList(json, fromJson: (json) => fromJson(json)));

  List<E> requireList<E>(String key, {FromJson<E>? fromJson}) =>
      require<List, List<E>>(key, (json) => fromJsonList(json, fromJson: fromJson));

  List<E> getList<E>(String key, {FromJson<E>? fromJson, List<E>? fallback}) =>
      opt<List, List<E>>(key, (json) => fromJsonList(json, fromJson: fromJson)) ?? fallback ?? <E>[];

  List<E>? optList<E>(String key, {FromJson<E>? fromJson}) =>
      opt<List, List<E>>(key, (json) => fromJsonList(json, fromJson: fromJson));

  Map<K1, V1> requireMap<K1, V1>(String key, {FromJson<K1>? fromJsonKey, FromJson<V1>? fromJsonValue}) =>
      require<Map, Map<K1, V1>>(
          key, (json) => fromJsonMap(json, fromJsonKey: fromJsonKey, fromJsonValue: fromJsonValue));

  Map<K1, V1> getMap<K1, V1>(String key,
          {FromJson<K1>? fromJsonKey, FromJson<V1>? fromJsonValue, Map<K1, V1>? fallback}) =>
      opt<Map, Map<K1, V1>>(key, (json) => fromJsonMap(json, fromJsonKey: fromJsonKey, fromJsonValue: fromJsonValue)) ??
      fallback ??
      <K1, V1>{};

  Map<K1, V1>? optMap<K1, V1>(String key, {FromJson<K1>? fromJsonKey, FromJson<V1>? fromJsonValue}) =>
      opt<Map, Map<K1, V1>>(key, (json) => fromJsonMap(json, fromJsonKey: fromJsonKey, fromJsonValue: fromJsonValue));

  E getRaw<E>(String key) {
    final value = this[key];
    if (value is E) return value;
    throw ArgumentError.value(value, key, 'expected to be of type $E');
  }

  T require<X, T>(String key, T Function(X json) transform) =>
      opt<X, T>(key, transform) ?? (throw ArgumentError.notNull(key));

  T? opt<X, T>(String key, T Function(X json) transform) {
    final value = this[key];
    if (value is X) return transform(value);
    if (value == null) return null;
    throw ArgumentError.value(value, key, 'expected to be of type $X');
  }
}
