extension SymbolExt on Symbol {
  String get name {
    final data = toString();
    return data.substring(8, data.length - 2);
  }
}
