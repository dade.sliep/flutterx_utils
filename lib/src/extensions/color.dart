import 'dart:math' as math;

import 'package:flutter/material.dart';

extension ColorExt on Color {
  /// Get HSLColor from this color
  HSLColor toHSLColor() => HSLColor.fromColor(this);

  /// Brightens the color by the given integer percentage amount from -1.0 to 1.0.
  Color brighten(double amount) => Color.fromARGB(alpha, (red + (255 * amount).round()).clamp(0, 255),
      (green + (255 * amount).round()).clamp(0, 255), (blue + (255 * amount).round()).clamp(0, 255));

  /// Blend in the given [input] Color with a percentage amount.
  Color blend(Color input, double amount) => amount == 0
      ? this
      : amount == 1
          ? input
          : Color.fromARGB(_lerpColor(alpha, input.alpha, amount), _lerpColor(red, input.red, amount),
              _lerpColor(green, input.green, amount), _lerpColor(blue, input.blue, amount));

  /// Compute RGB difference between this and other and returns delta
  double differenceRGB(Color other) =>
      math.sqrt(math.pow(other.red - red, 2) + math.pow(other.green - green, 2) + math.pow(other.blue - blue, 2));

  /// Get negative color in RGB scale
  Color get negative => Color.fromARGB(alpha, 255 - red, 255 - green, 255 - blue);

  /// returns brightness of this color based on luminance
  Brightness get brightness => ThemeData.estimateBrightnessForColor(this);

  /// returns HEX string representing this color e.g. FF00BB11
  String get hexString => value.toRadixString(16).padLeft(8, '0').toUpperCase();

  static int _lerpColor(int a, int b, double t) => (a + (b - a) * t).round().clamp(0, 255);
}
