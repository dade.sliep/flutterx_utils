extension ListExt<E> on List<E> {
  /// returns the element of this list that is next to [current].
  /// If current either the last element or is not in the list, returns the first element
  E nextElement(E current) => this[(indexOf(current) + 1) % length];

  /// Update the first element of this list that satisfies [test] predicate with [update]
  bool updateWhere(bool Function(E element) test, E Function(int index, E oldValue) update) {
    for (var i = 0; i < length; i++) {
      final value = this[i];
      if (test(value)) {
        this[i] = update(i, value);
        return true;
      }
    }
    return false;
  }

  /// Update all the elements of this list that satisfies [test] predicate with [update] function
  int updateAllWhere(bool Function(E element) test, E Function(int index, E oldValue) update) {
    var count = 0;
    for (var i = 0; i < length; i++) {
      final value = this[i];
      if (test(value)) {
        this[i] = update(i, value);
        count++;
      }
    }
    return count;
  }

  /// Update the first element of this list that satisfies [test] predicate with [newValue] or add to the list
  void upsertWhere(bool Function(E element) test, E newValue) =>
      updateWhere(test, (_, __) => newValue) ? null : add(newValue);
}

extension EnumValuesListExt<E extends Enum> on List<E> {
  E requireFromName(String name, {bool ignoreCase = false}) {
    final entry = optFromName(name, ignoreCase: ignoreCase);
    if (entry != null) return entry;
    throw ArgumentError.value(name, E.toString(), 'has no match with any of available values: [${join(', ')}]');
  }

  E fromName(String name, {required E fallback, bool ignoreCase = false}) =>
      optFromName(name, ignoreCase: ignoreCase) ?? fallback;

  E? optFromName(String name, {bool ignoreCase = false}) {
    if (ignoreCase) {
      name = name.toLowerCase();
      for (final entry in this) if (entry.name.toLowerCase() == name) return entry;
    } else {
      for (final entry in this) if (entry.name == name) return entry;
    }
    return null;
  }
}
