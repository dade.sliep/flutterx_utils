extension SetExt<E> on Set<E> {
  /// Toggle add/remove [value] from set
  /// Returns `true` if value has been added and `false` if removed
  bool toggleAdd(E value) => add(value) || !remove(value);
}
