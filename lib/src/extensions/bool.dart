bool parseBool(String source) {
  source = source.trim().toLowerCase();
  if (source == 'true') return true;
  if (source == 'false') return false;
  final x = num.tryParse(source);
  if (x != null) return x > 0;
  throw FormatException('Failed to convert value $source to bool');
}
