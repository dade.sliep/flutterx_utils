import 'package:flutter/foundation.dart';

extension IterableExt<E> on Iterable<E> {
  /// returns the first element that satisfies [test] of throws a [StateError] with given [message]
  E require(bool Function(E item) test, {required ValueGetter<String> message}) {
    for (final element in this) if (test(element)) return element;
    throw StateError(message());
  }

  /// Interpolates an [element] between every element of this iterable
  /// E.g [1, 2, 3, 4, 5].interpolate(0) -> [1, 0, 2, 0, 3, 0, 4, 0, 5]
  Iterable<E> interpolate(E element) sync* {
    final iterator = this.iterator;
    if (!iterator.moveNext()) return;
    yield iterator.current;
    while (iterator.moveNext()) {
      yield element;
      yield iterator.current;
    }
  }

  /// returns the count of [element] inside this iterable
  int count(E element) {
    var i = 0;
    for (final item in this) if (item == element) i++;
    return i;
  }

  /// returns the count of elements that satisfies [test] inside this iterable
  int countWhere(bool Function(E item) test) {
    var i = 0;
    for (final item in this) if (test(item)) i++;
    return i;
  }

  /// returns only elements of this iterable having a distinct [key]
  Iterable<E> distinct<K>(K Function(E item) key) sync* {
    final keys = <K>{};
    for (final element in this) if (keys.add(key(element))) yield element;
  }

  /// returns the element of this iterable having the lower [difference] between [other]
  E closestTo(E other, {num Function(E a, E b) difference = _closestToDifference}) {
    final iterator = this.iterator;
    if (!iterator.moveNext()) throw StateError('No element');
    late E result;
    num lastDifference = double.infinity;
    do {
      final value = iterator.current;
      final diff = difference(other, value).abs();
      if (diff < lastDifference) {
        if (diff == 0) return value;
        lastDifference = diff;
        result = value;
      }
    } while (iterator.moveNext());
    return result;
  }

  static num _closestToDifference(dynamic a, dynamic b) => a - b; // ignore: avoid_dynamic_calls

  List<E> merge(
    Iterable<E> source, {
    required bool Function(E oldItem, E newItem) condition,
    E Function(E oldItem, E newItem)? whenMatchedUpdate,
    E Function(E newItem)? whenNotMatchedInsert,
  }) {
    final result = [...this];
    for (final newItem in source) {
      var insert = true;
      for (var i = 0; i < result.length; i++) {
        final oldItem = result[i];
        if (condition(oldItem, newItem)) {
          result[i] = whenMatchedUpdate == null ? newItem : whenMatchedUpdate(oldItem, newItem);
          insert = false;
        }
      }
      if (insert) result.add(whenNotMatchedInsert == null ? newItem : whenNotMatchedInsert(newItem));
    }
    return result;
  }
}
