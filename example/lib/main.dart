import 'package:flutter/material.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutterx Utils Demo',
      theme: ThemeData(primarySwatch: Colors.orange),
      home: const LoggableExample());
}

class LoggableExample extends StatefulWidget {
  const LoggableExample({Key? key}) : super(key: key);

  @override
  State<LoggableExample> createState() => _LoggableExampleState();
}

class _LoggableExampleState extends State<LoggableExample> with Loggable {
  final List<String> _logs = [];
  double _value = 0;

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text('Loggable example')),
      body: Center(
        child: Wrap(children: [
          Text(_logs.join('\n')),
          _lightnessTest(Colors.pink.shade50),
          _lightnessTest(Colors.pink.shade200),
          _lightnessTest(Colors.pink.shade400),
          _lightnessTest(Colors.pink.shade500),
          _lightnessTest(Colors.pink.shade800),
          _lightnessTest(Colors.pink.shade900),
          _lightnessTest(Colors.black),
          _lightnessTest(Colors.grey),
          _lightnessTest(Colors.white),
          Text(_slideValue.toString()),
          Slider(value: _value, onChanged: (value) => setState(() => _value = value)),
        ]),
      ),
      floatingActionButton:
          FloatingActionButton(onPressed: () => logInfo('FAB pressed'), tooltip: 'Log', child: const Icon(Icons.add)));

  double get _slideValue => (_value - .5) * 2;

  Widget _lightnessTest(Color color) {
    final e = color.brighten(_slideValue);
    return Container(
      width: 100,
      height: 100,
      color: e,
      child: Center(child: Text('#${e.hexString}')),
    );
  }
}
