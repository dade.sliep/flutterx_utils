# flutterx_utils

A collection of Flutter utilities for any purpose.
Most of the utility functions you are searching are already present in flutter sdk.
Here you can find ONLY utility functions that flutter sdk does NOT provide

## Import

Import the library like this:

```dart
import 'package:flutterx_utils/flutterx_utils.dart';
```

## Usage

Check the documentation in the desired source file of this library